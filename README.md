# Postulacion Bice DevOps - Parte 1

## Repositorios de Infraestructura y Aplicación
Este documento resume un conjunto de repositorios que trabajan juntos para desplegar una aplicación en un clúster de Kubernetes en Azure utilizando Terraform y ArgoCD.

## Repositorio de Infraestructura (Terraforms AKS):
Este repositorio se encarga de la creación de la infraestructura necesaria para un clúster de Kubernetes en Azure. Utiliza Terraform para definir y proporcionar la infraestructura de manera declarativa. Además, contiene las instrucciones para levantar el servicio de ArgoCD en el clúster.

## Repositorio de la Aplicación (App-Repo):
Este repositorio contiene el código fuente de una aplicación NPM. La aplicación presenta una interfaz de usuario que consume la API de Evaluar. También incluye un flujo completo de integración continua y entrega continua (CI/CD) que se encarga de construir, probar y empaquetar la aplicación para su despliegue.

## Repositorio de Despliegue de la Aplicación (App-Deploy):
Este repositorio contiene los archivos de despliegue y servicio de Kubernetes para la aplicación. Estos archivos se modifican al final del flujo de CI/CD en el repositorio App-Repo para asegurar que la versión más reciente de la aplicación esté siempre disponible para su despliegue.

Estos cuatro repositorios trabajan juntos para proporcionar un flujo de trabajo completo de CI/CD que va desde la creación de la infraestructura hasta el despliegue de la aplicación, todo de forma automatizada y coherente.

## Diagrama Resumen

```mermaid
graph LR

A[IaC]
B[ArgoCD]
C[App-Repo]
D[App-Deploy]
E[AKS]
F[DockerHub]
G(Manifiestos/Archivos .yaml)

subgraph Infraestructura
A
B
E
end
subgraph CI/CD APP
C
D
G
end
A --> |Terraform| E 
B --> |Administra Apps con CD| E
C --> |Sube artifacts verificados|F
C --> |Actualización de manifiestos| D
D --> G
F --> |Apunta a latest arctifact con Tag de commit|G
B --> |Despliega a partir de|G
B --> |Revisa constantemente 'main'|D
```

### Disclaimer
Aún le falta mucho por pulir, como la creación de ArgoCD en Terraform y el despliegue de las aplicaciones directamente a través de Helm Chart u otra forma con mejor seguimiento y aprovechando IaC. Este conjunto de repositorios es el resultado de una noche de inspiración y mucho café. Fue entretenido volver a hacer cosas desde cero.